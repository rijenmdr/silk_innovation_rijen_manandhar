
import { useEffect, useState } from 'react';
import './App.css';
import Routes from './Routes'

function App() {
  const isLoggedIn = localStorage.getItem('access_token')
  return (
    <Routes isLoggedIn={isLoggedIn} />
  );
}

export default App;
