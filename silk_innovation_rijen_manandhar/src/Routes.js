import React, { useEffect, useState } from 'react'
import {Switch,Route,Redirect} from 'react-router-dom'
import Dashboard from './pages/Dashboard/Dashboard'
import Login from './pages/Login/Login'
import ProtectedRoute from './ProtectedRoutes'

const Routes=({isLoggedIn})=> {
    return (
        <Switch>
            <ProtectedRoute path="/dashboard" component={Dashboard}/>:
            <Route path="/login" render={
                    props => !isLoggedIn ?  <Login/>  : <Redirect to="/dashboard"/>
                } />
            <Route path="*" exact render={
                    props => !isLoggedIn ? <Redirect to="/login"/> : <Redirect to="/dashboard"/>
                } />
        </Switch>
    )
}

export default Routes
