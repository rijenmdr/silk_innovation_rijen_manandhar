import React from 'react'
import { useHistory } from 'react-router-dom'

const Nav = ({ openSidebar, setOpenSidebar, validUser }) => {
    const history = useHistory()
    const menuClick = () => {
        setOpenSidebar(prev => !prev)
    }

    const logoutUser = () => {
        localStorage.removeItem('access_token')
        localStorage.removeItem('user')
        history.push('/login')
    }

    return (
        <div className="nav-bar">
            <div className="nav-left">
                {
                    openSidebar ?
                        <span onClick={menuClick} class="material-icons-outlined menu">
                            menu_open
                        </span> :
                        <span onClick={menuClick} class="material-icons-outlined menu">
                            menu
                        </span>
                }
                <div className="logo">Logo</div>
                <div className="search-bar">
                    <input type="text" placeholder="Search..." />
                </div>
            </div>
            <div className="nav-right">
                <p className="total-balance">Total Balance<span className="balance">Rs {validUser.balance}</span></p>
                <span class="material-icons-outlined notification">notifications</span>
                <img className="avatar" src={`${validUser.avatar}`} alt={`${validUser.name}`} />
                <div className="name">
                    {validUser.name}
                </div>
                <span onClick={logoutUser} class="material-icons-outlined logout">logout</span>
            </div>
        </div>
    )
}

export default Nav
