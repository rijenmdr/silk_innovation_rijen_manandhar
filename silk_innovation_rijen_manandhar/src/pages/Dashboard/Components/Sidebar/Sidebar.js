import React from 'react'

const Sidebar = ({ openSidebar }) => {
    const sideBarItem = [
        { label: 'Home', icon: 'home', path: '/dashboard' },
        { label: 'Transaction', icon: 'receipt_long', path: '/transaction' },
        { label: 'My Wallet', icon: 'account_balance_wallet', path: '/mywallet' },
        { label: 'Account', icon: 'person', path: '/account' }
    ]
    return (
        <div className={`${openSidebar ? 'open-sidebar' : 'sidebar'}`}>
            {
                sideBarItem.map(item => (
                    <a className="item" href={item.path}>
                        <div class="material-icons-outlined icon">
                            {item.icon}
                        </div>
                        <div className="label">
                            {item.label}
                        </div>
                    </a>
                ))
            }
        </div>
    )
}

export default Sidebar
