import React from 'react'
import './Home.css'

const Home = ({ validUser }) => {
    const utilityItems = [
        {
            name: "Topup",
            logo: "smartphone"
        },
        {
            name: "Electricity",
            logo: "bolt"
        },
        {
            name: "Landline",
            logo: "call"
        }, {
            name: "Internet",
            logo: "wifi"
        }, {
            name: "Television",
            logo: "personal_video"
        }, {
            name: "Water",
            logo: "water_drop"
        }, {
            name: "Education",
            logo: "school"
        }, {
            name: "Flight Booking",
            logo: "flight_takeoff"
        }
    ]
    return (
        <div className="home-section">
            <div className="home-top">
                <div className="home-left">
                    <div className="actions">
                        <div className="card action-card">
                            <span class="material-icons-outlined load">
                                account_balance_wallet
                            </span>
                            <p>Load Money</p>
                        </div>
                        <div className="card action-card">
                            <span class="material-icons-outlined transfer">
                                account_balance
                            </span>
                            <p>Bank Transfer</p>
                        </div>
                        <div className="card action-card">
                            <span class="material-icons-outlined send">
                                send
                            </span>
                            <p>Send Funds</p>
                        </div>
                    </div>
                    <div className="banner">
                        <div className="card">
                            <img src="https://nepallife.com.np/storage/settings/nepallife-logo.jpg" />
                        </div>
                    </div>
                </div>
                <div className="home-right">
                    <div className="card">
                        <p className="profile"> My Profile</p>
                        <img src={`${validUser.avatar}`} />
                        <p className="user-name">{validUser.name}</p>
                        <div className="action-btn">
                            <button><span class="material-icons-outlined">edit</span></button>
                            <button>View Full Profile</button>
                        </div>
                    </div>
                </div>
            </div>
            <div className="home-bottom">
                <div className="card">
                    <p className="title">Utility Payment</p>
                    <div className="utility-items">
                        {
                            utilityItems.map((item, key) => (
                                <div className="utility-item">
                                    <span class="material-icons-outlined logo">
                                        {item.logo}
                                    </span>
                                    <div className="name">{item.name}</div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home
