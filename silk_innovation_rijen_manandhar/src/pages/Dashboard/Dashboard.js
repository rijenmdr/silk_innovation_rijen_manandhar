import React, { useState } from 'react'
import Nav from './Components/Nav/Nav'
import Sidebar from './Components/Sidebar/Sidebar'
import Home from './Container/Home.js/Home'
import './Dashboard.css'

const Dashboard = () => {
    const [openSidebar, setOpenSidebar] = useState(false)
    const validUser = JSON.parse(localStorage.getItem('user'))
    return (
        <div className="dashboard">
            <Nav
                openSidebar={openSidebar}
                setOpenSidebar={setOpenSidebar}
                validUser={validUser}
            />
            <div className="content">
                <Sidebar openSidebar={openSidebar} />
                <Home
                    validUser={validUser}
                />
            </div>
        </div>
    )
}

export default Dashboard
