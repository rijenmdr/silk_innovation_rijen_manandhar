import React, { useEffect, useState } from 'react'
import { userLogin } from './LoginApi'
import './Login.css'
import { useHistory } from 'react-router-dom'

const Login = () => {
    const history = useHistory()
    const [isPasswordHidden, setIsPasswordHidden] = useState(true)
    const [isDisabled, setIsDisabled] = useState(true)
    const [loading, setLoading] = useState(false)
    const [value, setValue] = useState({
        email: '',
        mobile_no: '',
        password: '',
        pin: ''
    })
    const [error, setError] = useState({
        email_phone: '',
        password_pin: ''
    })
    const [loginError, setLoginError] = useState('')

    const showHidePassword = () => {
        setIsPasswordHidden(prev => !prev)
    }

    useEffect(() => {
        if ((value.email || value.mobile_no) && (value.password || value.pin)) {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }
    }, [value])

    const inputHandler = (e) => {
        if (e.target.name === 'email_phone') {
            const phoneValidation = /^9(61|62|72|74|75|80|81|82|84|85|86|88)[0-9]{7}$/;
            const mailValidation = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (e.target.value.match(phoneValidation)) {
                setValue({
                    ...value,
                    email: '',
                    mobile_no: e.target.value
                })
                setError({ ...error, email_phone: '' })
            } else if (e.target.value.match(mailValidation)) {
                setValue({
                    ...value,
                    mobile_no: '',
                    email: e.target.value
                })
                setError({ ...error, email_phone: '' })
            } else if (e.target.value.length === 0) {
                setError({ ...error, email_phone: 'This field is required' })
                setIsDisabled(true)
            }
            else {
                setError({ ...error, email_phone: 'Invalid Email/Phone No' })
                setIsDisabled(true)
            }
        } else {
            const pinValidation = /^(\d{4})$/
            if (e.target.value.match(pinValidation)) {
                setValue({
                    ...value,
                    pin: e.target.value,
                    password: ''
                })
            } else if (e.target.value === '') {
                setError({ ...error, password_pin: 'This field is required' })
                setIsDisabled(true)
            } else {
                setValue({
                    ...value,
                    password: e.target.value,
                    pin: ''
                })
                setError({ ...error, password_pin: '' })
            }
        }
    }

    const submitHandler = (e) => {
        e.preventDefault()
        setLoginError('')
        setLoading(true)
        userLogin(value).then(response => {
            if (response && response.status === 200) {
                setLoading(false)
                history.push('/dashboard')
            } else {
                setLoading(false)
                setLoginError(response)
            }
        })
    }
    return (
        <div className="container">
            {loading &&
                <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            }
            <div className="login">
                <div className="title">Login to your account</div>
                {loginError &&
                    <div className="login-error">{loginError}</div>
                }
                <div className="login-form">
                    <div className="form-field">
                        <label>Email/Phone</label>
                        <input type="text"
                            className={error && error.email_phone && 'border-red'}
                            name="email_phone"
                            placeholder="Enter Your Email/Phone No"
                            onChange={(e) => inputHandler(e)} />
                        {error && error.email_phone &&
                            <div className="error">{error.email_phone}</div>
                        }
                    </div>
                    <div className="form-field">
                        <label>Password/Pin</label>
                        <input
                            type={isPasswordHidden ? `password` : `text`}
                            className={error && error.password_pin && 'border-red'}
                            name="password_pin"
                            placeholder="Enter Your Password/Pin"
                            onChange={(e) => inputHandler(e)}
                        />
                        {
                            isPasswordHidden ?
                                <span className="material-icons-outlined eye-btn" onClick={showHidePassword}>visibility_off</span> :
                                <span className="material-icons-outlined eye-btn" onClick={showHidePassword}>visibility</span>
                        }
                        {error && error.password_pin &&
                            <div className="error">{error.password_pin}</div>
                        }
                    </div>
                    <button
                        disabled={isDisabled}
                        className="login-btn"
                        onClick={submitHandler}>Login</button>
                </div>
            </div>
        </div>
    )
}

export default Login
