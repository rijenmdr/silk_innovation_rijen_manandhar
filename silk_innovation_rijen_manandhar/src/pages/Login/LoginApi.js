import axios from 'axios'

export const userLogin = async (value) => {
    let responseMessage
    let parameter;
    if (value.email && value.password) {
        parameter = {
            email: value.email,
            password: value.password,
            fcm_token: 'no_fcm'
        }
    } else if (value.email && value.pin) {
        parameter = {
            email: value.email,
            pin: value.pin,
            desktop_fcm: 'no_fcm'
        }
    } else if (value.mobile_no && value.password) {
        parameter = {
            mobile_no: value.mobile_no,
            password: value.password,
            desktop_fcm: 'no_fcm'
        }
    } else {
        parameter = {
            mobile_no: value.mobile_no,
            pin: value.pin,
            desktop_fcm: 'no_fcm'
        }
    }
    const headers = {
        'App-Authorizer': 647061697361,
        'Content-Type': 'application/json'
    }
    await axios.post(`https://stagingapi.icash.com.np/api/login`, parameter, { headers }).then(response => {
        const data = response.data
        localStorage.setItem('access_token', data?.access_token)
        const user = {
            name: data?.user?.name,
            email: data?.user?.email,
            avatar: data?.user?.avatar,
            balance: data?.user?.balance
        }
        localStorage.setItem('user', JSON.stringify(user))
        responseMessage = response
    }).catch(error => {
        if (error.response) {
            responseMessage = error.response.data.message
        }
        else {
            responseMessage = error.message
        }
    }
    )
    return responseMessage
}
